meteor add urigo:angular2-meteor
meteor remove blaze-html-templates
meteor remove jquery
meteor remove ecmascript
meteor add pbastowski:angular-babel

meteor remove insecure
meteor remove autopublish

meteor add accounts-password
meteor add barbatus:ng2-meteor-accounts
meteor add barbatus:ng2-meteor-accounts-ui
meteor add email
meteor add kriegslustig:cssnext

meteor add tsumina:meteor-systemjs