/**
 * Clean Mongo database default data loading.
 * @copyright ByteCubed 2016
 * @author Justin Williams
 * @module server/init/load-from-new
 * 
 * @rationale
 * This is a place to put ALL the initial data loading of a clean Mongo database, whenever necessary.
 * In this case, we want to make sure we add some tiles in since there's no way in the UI to do that.
 * Try meteor reset and changing the size of the for loop below to see the impact of fewer/more tiles appearing on the game board.
 * The name of this file includes .sys. This is how tsumina:meteor-systemjs finds the file server side so we can use the same import/export syntax for both.
 * It is unnecessary to name js files on the client with the .sys. convention. This will change in future releases.
 */


import {GameTiles} from 'collections/game-tiles';

export function initData() {
    if (GameTiles.find().count() === 0) {
        
        for (var i = 0; i < 30; i++) {
            let tile = {
                position: [0, 0, 0],
                rotation: [0, 0, 0],
                duration: 0
            }
            GameTiles.insert({layout: tile, color: Math.round(Math.random() * 3)});
        }
  
    }
}
