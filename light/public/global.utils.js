/**
 * A repository for useful global utilities such as generating a pretty good (fast) guid.
 * @copyright ByteCubed 2016
 * @author Justin Williams
 * 
 * @rationale
 * An ES5 repository of simple things that should probably be a part of Javascript, globally scoped under 'globalUtils'.
 * In the future there should be container detection but for now this is browser dependent.
 * tryNonRecursiveUpdates should be replaced with something far better than a 0 length setTimeout but needed something quickly to avoid double binding setting (a common problem to any framework).
 */

if(!globalUtils) {
    var globalUtils = {
        guid: function guid() {
            return 'idxxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx'.replace(/x/g, function (c) {
                var r = Math.random() * 16 | 0,
                    v = c == 'x' ? r : r & 0x3 | 0x8;return v.toString(16);
            });
        },
        tryNonRecursiveUpdates: function(f, t) {
            t = t || 0;
            setTimeout(f, t);
        }
    };
}