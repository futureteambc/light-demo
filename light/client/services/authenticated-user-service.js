/**
 * A data service providing access to the currently authenticated user.
 * @copyright ByteCubed 2016
 * @author Justin Williams
 * @module client/services/authenticated-user-service
 * 
 * @rationale
 * This service is really not necessary but is done as a simple demonstration of the boilerplate for a service and a strategy for grabbing just 'one thing'.
 * Currently, you can't use @Inject in the constructor in ES6 so we define the constructor parameter types through the static 'parameters' getter.
 * There are two different types of reactive trackers, the Meteor one for the cursor and the Angular zone for template reactivity.
 * There's probably a nice clean way to combine those two, however for now we next the zone in the cursor tracker for full reactivity.
 */

import {ChangeDetectorRef, Injectable, NgZone} from 'angular2/core';

@Injectable()
export class AuthenticatedUserService {
    static get parameters() {
        return [[NgZone]];  
    }      

    constructor(_zone) {
        this.loggedIn = false;
        this.name = "";
        this.verified = false;

        Tracker.autorun(() => _zone.run(() => {
            this._result = Meteor.users.findOne();
            if(this._result && this._result.emails) {
                this.loggedIn = true;
                this.name = this._result.emails[0].address;
                this.verified = this._result.emails[0].verified;
            } else {
                this.loggedIn = false;
                this.name = "";
                this.verified = false;
            }
        }));
    }
}