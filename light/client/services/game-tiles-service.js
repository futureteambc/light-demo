/**
 * A data service providing access to the currently authenticated user.
 * @copyright ByteCubed 2016
 * @author Justin Williams
 * @module client/services/game-tiles-service
 * 
 * @rationale
 * This service builds on the template and boilerplate code from authenticated-user-service which explains more of those aspects.
 * We only want to persist the color of GameTiles for now but the GameTile service handles all of the GameTile data needs, including those that we expressly want to keep client side.
 * Examples of client side only data include the current transforms (translation and rotation).
 * If it weren't for that we would simply bind to the Meteor cursor in our templates and .subscribe(), but as it is we need to set up trackers because we add data to each returned object.
 * For the array data we use, we don't want to replace the entire collection every time one tile's color changed, so we do some quick and dirty iterating to change only what came from the database.
 * Updating shows some nice Mongo style syntax. It's still better to encapsulate any updates into a method on this class than from the views though since we then don't have to track down everywhere it's done if we need to add business logic down the road.
 */

import {ChangeDetectorRef, Injectable, NgZone} from 'angular2/core';
import {GameTiles} from 'collections/game-tiles';

@Injectable()
export class GameTilesService {
    static get parameters() {
        return [[NgZone]];  
    }      

    constructor(_zone) {
        this.tiles = [];
        this.tilesCursor = null;
        
        Meteor.subscribe(GameTiles.NAME);
        Tracker.autorun(() => _zone.run(() => {
            this.tilesCursor = GameTiles.find();
            let t = this.tilesCursor.fetch();
            if(! this.tiles || this.tiles.length != t.length) {
                this.tiles = t;
            } else {
                // Transform and merge the tiles with UI specific state (position, rotation, etc) with the data we want to persist without refreshing the whole array.
                for (let i = 0; i < t.length; i++) {
                    this.tiles[i].color = t[i].color;
                }
            }
        }));

    }
    
    updateColor(id, colorIndex) {
        GameTiles.update(id, { $set: {color: colorIndex }});
    }
}