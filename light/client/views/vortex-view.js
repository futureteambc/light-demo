/**
 * An implementation of the game's playinng field, a spinning vortex of cards to win.
 * @copyright ByteCubed 2016
 * @author Justin Williams
 * @module client/views/vortex-view
 * 
 * @rationale
 * This is an application specific view wired up to meet the requirements of this app.
 * Because it is a specific view based on a particular wireframe or "comp"/"mockup", contains plenty of very specific business logic.
 * For a broader desciption of the game mechanics see the login-view rationale.
 * Here we borrow the Snabbt cylinder demo with minor tweaks for a nice, constantly rotating game board and tiles laid out evenly based on a few dimensions and quantity.
 * Because transform-style: preserve-3d doesn't work in IE yet (as of 11) and because we're lazy and rotating in 3d separately from laying out tiles, this only works in .. every other browser.
 * Without a semi-transparent "fog of war" background the tile colors blend together so a mid-point backplane is added. Alternatively we could not use full opacity on the tiles and that would probably work as well.
 * Today, you can only transform x, y, z, rotation, and opacity with hardware accelerated speeds. As such, to change color, it's actulaly faster to have multiple colored planes fade in and out than one plane directly animate its color.
 * We add one card just to measure the vortex's overall size because all the dramatic transforms mess with size events of the actual platform in some browsers.
 * We need the size of the viewport in order to know how to lay out the tiles correctly.
 * This is a pretty heavy view. At some point, if it were to get too much more complex, we might consider pulling out certain aspects into their own sub-views or components.
 * Currently, you can't use @Inject in the constructor in ES6 so we define the constructor parameter types through the static 'parameters' getter.
 */

import {Component, View} from 'angular2/core';

import {Card} from 'client/controls/card'

import {AccountsUI} from 'meteor-accounts-ui';

import {SizeSpyDirective} from 'client/directives/size-spy';

import {GameTilesService} from 'client/services/game-tiles-service';


@Component({
    selector: 'vortex-view',
    inputs: ['teamColorIndex'],
    providers: [GameTilesService]
    })
@View({
    styles: [`
        :host {
            display: block;
            background-color: rgba(235, 240, 255, 0.6);
            width: 100%;
            height: 100%;
        }
        .card-layout {        
            position: absolute;
            transform-style: preserve-3d;
        }
        .spy {
            position: absolute; 
            z-index: -100; 
            min-width: 100%; 
            min-height: 100%;            
        }
    `],
    template: `
        <accounts-ui></accounts-ui>
        <h2>Go {{_teamColors[teamColorIndex].name}}!</h2>
        
        <card   [sizeSpy]="onViewResize" class="spy"></card>

        <card   [transforms]="platformConfig" 
                class="card-layout" [style.minWidth]="'100px'" [style.minHeight]="'100px'"
                [style.top]="platformTop + 'px'" [style.right]="platformRight + 'px'" [style.bottom]="platformBottom + 'px'" 
                [style.left]="platformLeft + 'px'">
            <card class="card-layout" *ngFor="#tile of gameTiles.tiles" [transforms]="tile.layout" (click)="onTileClick(tile)" style="width: 50px; height: 150px;">
                <card class="card-layout" [transforms]="tile.color == 0 ? opacitySolid : opacityTransparent">
                    <div style="width: 100%; height: 100%; background-color: blue"></div>
                </card>
                <card class="card-layout" [transforms]="tile.color == 1 ? opacitySolid : opacityTransparent">
                    <div style="width: 100%; height: 100%; background-color: green"></div>
                </card>
                <card class="card-layout" [transforms]="tile.color == 2 ? opacitySolid : opacityTransparent">
                    <div style="width: 100%; height: 100%; background-color: red"></div>
                </card>
                <card class="card-layout" [transforms]="tile.color == 3 ? opacitySolid : opacityTransparent">
                    <div style="width: 100%; height: 100%; background-color: purple"></div>
                </card>
            </card>
        </card>
        `,
    directives: [AccountsUI, Card, SizeSpyDirective]
})
export class VortexView {
    static get parameters() {
        return [[GameTilesService]];  
    }
    
    get teamColorIndex() {
        return this._teamColorIndex;
    }
    set teamColorIndex(value) {
        if(value != this._teamColorIndex) {
            this._teamColorIndex = value;
        }
    }
    
    onTileClick(t) {
        if (t.color != this.teamColor) {
            this.gameTiles.updateColor(t._id, this.teamColorIndex);
        }
    }
    
    
    constructor(_tileService) {
        this._teamColorIndex = 0;
        this._teamColors = [
            {name: "Blue Team",     value: "0"},
            {name: "Green Team",    value: "1"},
            {name: "Red Team",      value: "2"},
            {name: "Purple Team",   value: "3"}
        ];
        
        this._platformDetails = {
          width: 1,
          height: 1  
        };
        this.platformTop = 70;
        this.platformBottom = 0;
        this.platformLeft = 0;
        this.platformRight = 0;
        
        // Resize events can often happen dozens of times in a row if a window is dragged bigger, for instance.
        // We put a delay on reacting to resizes for both effeciency and dramatic effect.
        this._sizeReadyForChange = true;
        this._accumulateResizes = () => {
            if (this._sizeReadyForChange) {
                this._sizeReadyForChange = false;
                setTimeout(() => {this._accumulateResizes(); this._sizeChangeRegistered = false; }, 1000);
            } else {
                this.setupVortex();
            }
        };

        this.onViewResize = (event) => {
            if (event) {
                // setupVortex() is an expensive operation so only do if something important has actually changed.
                if (this._platformDetails.width != event.width || this._platformDetails.height != event.height) {
                    this._platformDetails.width = event.width;
                    this._platformDetails.height = event.height;
                    this._sizeReadyForChange = true;
                    setTimeout(this._accumulateResizes, 1000);
                }
            }
        };
        
        this.opacitySolid = {
          opacity: 1,
          fromOpacity: 0,
          easing: 'ease',
          duration: 500  
        };
        
        this.opacityTransparent = {
          opacity: 0,
          fromOpacity: 1,
          easing: 'ease',
          duration: 500  
        };

        this.gameTiles = _tileService;
        
        this.setupRotation();
    }
    
    setupVortex() { 
        // left & right 8% of total width, min of 8px.
        this.platformLeft = this._platformDetails.width > 100 ? this._platformDetails.width * .1 : 10;
        
        if (this.platformRight !== this.platformLeft) {
            this.platformRight = this.platformLeft;

            // top & bottom 10% of total height, min of 10px.
            this.platformTop = this._platformDetails.height > 100 ? this._platformDetails.height * .1 : 10;
            this.platformBottom= this.platformTop;

            let cardH = 150;
            let start_x = (this._platformDetails.width - this.platformLeft * 2) / 2;
            let start_y = (this._platformDetails.height - this.platformTop * 2) * 0.1;
            let radius = (this._platformDetails.width - this.platformLeft * 2) * 0.2;
            
            let tiles = this.gameTiles.tiles;

            if (tiles) {
                for (var i = 0; i < tiles.length; i++) {
                    let angle = i % 10 / 10 * 2 * Math.PI;
                    let x = Math.cos(angle) * radius + start_x;
                    let z = Math.sin(angle) * radius;
                    let y = Math.floor(i / 10) * 1.2 * cardH + start_y;

                    tiles[i].layout = {
                        position: [x, y, z],
                        rotation: [0, Math.PI / 2 + angle, 0],
                        easing: 'ease',
                        delay: i * 50
                    }
                }
            }
        }

    }

    setupRotation() {
        this.platformConfig = {
                fromRotation: [0, 0, 0],
                rotation: [0, 2 * Math.PI, 0],
                duration: 20000,
                perspective: 2000,
                complete: () => { this.setupRotation() }
            };
    }

}