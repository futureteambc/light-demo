/**
 * Provides a way to determine if one particular container changes size.
 * @copyright ByteCubed 2016
 * @author Justin Williams
 * @module client/directives/size-spy
 * 
 * @rationale
 * It's easy to see if the window changed size but what if an element programatically changed within the window that effects important layout?
 * This directive can help provide a more robust form of size change detection through use of a hidden <object> tag.
 * HTML does not provide a resize event on arbitrary containers for a reason, please use this directive sparingly for performance reasons.
 */

import {Directive, ElementRef} from 'angular2/core';

@Directive({
  selector: '[sizeSpy]',
  providers: [ElementRef],
  inputs: ['sizeSpy']
})
export class SizeSpyDirective {

    static get parameters() {
        return [[ElementRef]];  
    }      
    
    get sizeSpy() {
        return this._callback;
    }
    set sizeSpy(value) {
        this._callback = value;
        if (value) {
            value( {
                    'width': this._element.nativeElement.offsetWidth,
                    'height': this._element.nativeElement.offsetHeight
                   });
            this._spy.data = "about:blank";
        }
    }

    constructor(_el) {
        this._callback = null;
        this._element = _el;
        this._onSpyResize = (e) => {
            if (this.sizeSpy) {
                this.sizeSpy( {
                    'width': this._element.nativeElement.offsetWidth,
                    'height': this._element.nativeElement.offsetHeight
                    } );
            }
        };
        window.debugTest = _el;

        this._spy = document.createElement('object');
        this._spy.setAttribute('style', `
            position: absolute;
            visibility: hidden;
            top: 0px;
            left: 0px;
            width: 100%;
            height: 100%;
            pointer-events: none;
            z-index: -99;
        `);
        this._spy.__callback = this._onSpyResize;
        this._spy.onload = function(e) {
            this.contentDocument.defaultView.addEventListener('resize', this.__callback);
            };
        this._spy.type = "text/html";
        this._element.nativeElement.appendChild(this._spy);
    }
}
