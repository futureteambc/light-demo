/**
 * An icon based drop down menu for selecting application state.
 * @copyright ByteCubed 2016
 * @author Justin Williams
 * @module client/controls/icon-menu
 * 
 * @rationale
 * An overly fancy icon menu for demonstrating control building.
 * It takes a check box and radio group and turns it into a menu fully driven by data and human interaction.
 * Despite its flair there's much to be improved, such as an option to have clicking outside of it collapse it and useful, robust menuAlign.
 */

import {Component, View, EventEmitter} from 'angular2/core';
import {SnabbtDirective} from 'client/directives/snabbt-data';

@Component({
    selector: 'icon-menu',
    inputs: ['selectedItem', 'menuItems', 'menuAlign'],
    outputs: ['selectedItemChange']
    })
@View({
    templateUrl: 'client/controls/icon-menu.html',
    styleUrls:   ['client/controls/icon-menu.next.css'],
    directives: [SnabbtDirective]
})
export class IconMenu {
    get selectedItem() {
        if(this._selectedItem === undefined) {
            this._selectedItem = "0";
        }
        return this._selectedItem;
    }
    set selectedItem(value) {
        if(value != this._selectedItem) {
            this._selectedItem = String(value);
            this.selectedItemChange.next(value);
        }
    }
    constructor() {
        this.toggle = false;
        this.selectedItemChange = new EventEmitter();
        this.revealAnimation = {
            opacity: 1,
            fromOpacity: 0,
            rotation: [0, 0, 0],
            duration: 700,
            transformOrigin: [-50, 0, 0],
            easing: "easeOutBounce"
        };
        this.hideAnimation = {
            duration: 850,
            rotation: [0, 2*Math.PI, 0],
            fromOpacity: 1,
            opacity: 0,
            transformOrigin: [-50, 0, 0],
            easing: "easeOut"
        };
    }
}