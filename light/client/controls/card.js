/**
 * An arbitrary view wrapper desinged to work with show/hide containers like the CardDeck.
 * @copyright ByteCubed 2016
 * @author Justin Williams
 * @module client/controls/card
 * 
 * @rationale
 * Cards are fairly dumb wrappers to arbitrary template content that know how to hide and show themselves.
 * They also know how to animate through a transforms property which is based on snabbt json animation definitions.
 */

import {Component, View, EventEmitter} from 'angular2/core';
import {SnabbtDirective} from 'client/directives/snabbt-data';

@Component({
    selector: 'card',
    inputs: ['transforms', 'visible'],
    outputs: ['visibleChange'],
    host: { "[hidden]": "! visible"}
    })
@View({
    styles: [`
        :host, .card-defaults
        {
            position: absolute;
            top: 0px;
            bottom: 0px;
            left: 0px;
            right: 0px;
            min-width: 100px;
            min-height: 100px;
        }
        .card-defaults: 
        {
            width: 100%;
            height: 100%;
        }
    `],
    template: `
        <div class="card-defaults" [snabbtData]="transforms">
                <ng-content></ng-content>
        </div>`,
    directives: [SnabbtDirective]
})
export class Card {
    get visible() {
        return this._visible;
    }
    set visible(value) {
        this._visible = value;
        this.visibleChange.next(value);
    }
    
    constructor() {
        this.visibleChange = new EventEmitter();
        this.transforms = null;
        this.visible = true;
    }
}